--*************************************
--*** GLOBAL VARS *********************
--*************************************

--sizes
local largeur = 0
local hauteur = 0
local shipW = 0
local shipH = 0

--sprites
local ship1
local ship2
local ship3
local laser 
local fond
local fond1
local fond2


--hardware
local joysticks
local joyCount
local joystick1

--movement ship
local shipFixX = 0
local shipFixY = 0
local shipX = 0
local shipY = 0
local xc = 0
local dx = 0
local dy = 0
local lastbutton = "none"
local ldt = 0
local speed

--movement background

local spaceHW = 1024;

local spaceX = 0
local spaceY = 0

local spaceX1 = 0
local spaceY1 = 0
local spaceX2 = 0
local spaceY2 = 0
local spaceX3 = 0
local spaceY3 = 0

local spaceX4 = 0
local spaceY4 = 0
local spaceX6 = 0
local spaceY6 = 0

local spaceX7 = 0
local spaceY7 = 0
local spaceX8 = 0
local spaceY8 = 0
local spaceX9 = 0
local spaceY9 = 0


local spaceDy = 0
local spaceDy2 = 0
local rot = 0
local rotShip = 0;

--laser
local laserW = 0
local laserX = {}
local laserY = {}
local laserIndex = 0


--sound
local music 
local fire1 
local fire2 
local frame = 0;


--*************************************
--*** LOAD ****************************
--*************************************
function love.load()

	--ARRAY INITIALISATION FOR THE LASERS
    for i=0, 9 do
      laserX[i] = 0
      laserY[i] = 0
    end

	--love.window.setMode(1024, 768, {resizable=false, vsync=false, minwidth=400, minheight=300})

	--LOAD SPRITES
	ship1 = love.graphics.newImage("ship1.png");
   	ship2 = love.graphics.newImage("ship2.png");
   	ship3 = love.graphics.newImage("ship3.png");
	laser = love.graphics.newImage("laser.png");
	fond = love.graphics.newImage("space-pattern.jpg");
	fond1 = love.graphics.newImage("grid.png");
	fond2 = love.graphics.newImage("space-pattern3.png");

    --CALCULATES CONSTANTS
	largeur = love.graphics.getWidth()
	hauteur = love.graphics.getHeight()   
    shipW = ship1:getWidth()
    shipH = ship1:getHeight()

    laserW = laser:getWidth()
    
	--SHIP POSITION AT START
	shipFixX = largeur/2
	shipFixY = 0.75 * hauteur
    
	shipX = largeur/2
	shipY = 0.75 * hauteur

    
	joysticks = love.joystick.getJoysticks( )
	joyCount = # joysticks;
	--joyCount = love.joystick.getJoystickCount()
	--axisDir1, axisDir2, ..., axisDirN = Joystick:getAxes()
	joystick1 = nil

    --SOUNDS
	music =  love.audio.newSource("anewdimension.mp3", "static") 
	fire1 =  love.audio.newSource("pulsfire.wav", "static")  
	fire2 =  love.audio.newSource("sushitwt.wav", "static")  
    
	music:setVolume(0.5)
	music:play()
    
	print("loaded!")

end

--***************************************************
--*** JOYSTICK ADDED ********************************
--***************************************************

function love.joystickadded(joystick)
    joystick1 = joystick
    
end

--***************************************************
--*** GAMEPAD BUTTONS *******************************
--***************************************************
function love.gamepadpressed(joystick, button)
    lastbutton = button

   	if button == "a" then  
        fireLaser()
	elseif button == "b" then
        fireMissile()
	end
	
end

--***************************************************
--*** KEYBOARD **************************************
--***************************************************
function love.keypressed( key, scancode, isrepeat )
    if key == "lctrl" or key == "rctrl" then
       fireLaser()
    elseif key == "space" then
       fireMissile()
    end
    
end

--***************************************************
--*** MY ACTIONS ************************************
--***************************************************

function fireLaser()
    fire1:stop()
    fire1:play()

    --FIRE LASER!
    laserIndex = laserIndex + 1;
    if laserIndex > 9 then laserIndex = 0 end
    laserX[laserIndex] = shipFixX
    laserY[laserIndex] = shipFixY
end 

function fireMissile()
	fire2:stop()
	fire2:play()
end

--***************************************************
--*** UPDATE ! ************************************** 
--***************************************************
function love.update(dt)
	frame = frame + 1
	if(frame > 200) then
		fond = fond1 --to explain how it work
	end
    ldt	= dt
	
	dx = 0
	dy = 0
	
    if love.keyboard.isDown('a') or  love.keyboard.isDown('left')  then 
        dx = -1
    end

    if love.keyboard.isDown('d') or love.keyboard.isDown('right') then 
        dx = 1
    end

--    if love.keyboard.isDown('w') then
--        dy = 1
--    end
--    if love.keyboard.isDown('s') then
--        dy = -1
--    end
    
	rot = rot + 2 * dt * dx

    if joystick1 ~= nil then
        -- getGamepadAxis returns a value between -1 and 1.
        -- It returns 0 when it is at rest
		dx = joystick1:getGamepadAxis("leftx")
		--dy = joystick1:getGamepadAxis("lefty")

		--PREVENT SUBTLE 0 ERROR OR BAD CALIBRATION
		if dx > -0.2 and dx < 0.2  then dx = 0 end 
		--if dy > -0.2 and dy < 0.2  then dy = 0 end

		rot = rot + 3 * dt * dx
        --x = x + 300 * dt * dx --MOVE THE SHIP IN X DIRECTION
        --y = y + 300 * dt * dy --MOVE THE SHIP IN Y DIRECTION
    end

	rotShip = rot + 5 * math.pi / 4
	speed = 300 * dt

	shipX = (shipX + speed * math.cos(rotShip) - speed * math.sin(rotShip) ) % spaceHW
	shipY = (shipY + speed * math.sin(rotShip) + speed * math.cos(rotShip) ) % spaceHW

	
	--I compose three transformations:
	-- A translation that brings a point to the origin
	-- Rotation around the origin
	-- A translation that brings back the point to its original position
	--I work this all out, which give the following transformations:
	-- newX = centerX + (pointX-centerX)*Math.cos(x) - (pointY-centerY)*Math.sin(x);
	-- newY = centerY + (pointX-centerX)*Math.sin(x) + (pointY-centerY)*Math.cos(x);

    local cosr = math.cos(-rot);
    local sinr = math.sin(-rot);
    
	spaceX = shipFixX + (0-shipX) * cosr - (0-shipY) * sinr
	spaceY = shipFixY + (0-shipX) * sinr + (0-shipY) * cosr
    
	spaceX1 = shipFixX + (-spaceHW-shipX) * cosr - (-spaceHW-shipY) * sinr
	spaceY1 = shipFixY + (-spaceHW-shipX) * sinr + (-spaceHW-shipY) * cosr
    spaceX2 = shipFixX + (0-shipX) * cosr - (-spaceHW-shipY) * sinr
	spaceY2 = shipFixY + (0-shipX) * sinr + (-spaceHW-shipY) * cosr
	spaceX3 = shipFixX + (spaceHW-shipX) * cosr - (-spaceHW-shipY) * sinr
	spaceY3 = shipFixY + (spaceHW-shipX) * sinr + (-spaceHW-shipY) * cosr

	spaceX4 = shipFixX + (-spaceHW-shipX) * cosr - (0-shipY) * sinr
	spaceY4 = shipFixY + (-spaceHW-shipX) * sinr + (0-shipY) * cosr
	spaceX6 = shipFixX + (spaceHW-shipX) * cosr - (0-shipY) * sinr
	spaceY6 = shipFixY + (spaceHW-shipX) * sinr + (0-shipY) * cosr

	spaceX7 = shipFixX + (-spaceHW-shipX) * cosr - (spaceHW-shipY) * sinr
	spaceY7 = shipFixY + (-spaceHW-shipX) * sinr + (spaceHW-shipY) * cosr
    spaceX8 = shipFixX + (0-shipX) * cosr - (spaceHW-shipY) * sinr
	spaceY8 = shipFixY + (0-shipX) * sinr + (spaceHW-shipY) * cosr
	spaceX9 = shipFixX + (spaceHW-shipX) * cosr - (spaceHW-shipY) * sinr
	spaceY9 = shipFixY + (spaceHW-shipX) * sinr + (spaceHW-shipY) * cosr
    

	--MOVE THE LASERS
	--TODO: TRANSFORM THE LASERS FROM THEIR ORIGIN WHERE THEY WHERE FIRED
    for i=0, 9 do
		if laserY[i] > -1 then  
	      laserY[i] = laserY[i] - 600 * dt;
		end
    end

	--MUSIC PITCH EFFECT
	if( math.abs(dy) > math.abs(dx)) then
    	music:setPitch(1-0.2*dy)
    else 
    	music:setPitch(1)
	end

end
 



--***************************************************
--*** DRAW ! **************************************** 
--***************************************************
function love.draw()  

	--SPACE BACKGROUND
	love.graphics.draw(fond, spaceX, spaceY, -rot)

	love.graphics.draw(fond, spaceX1, spaceY1, -rot)
	love.graphics.draw(fond, spaceX2, spaceY2, -rot)
	love.graphics.draw(fond, spaceX3, spaceY3, -rot)

	love.graphics.draw(fond, spaceX4, spaceY4, -rot)
	love.graphics.draw(fond, spaceX6, spaceY6, -rot)

	love.graphics.draw(fond, spaceX7, spaceY7, -rot)
	love.graphics.draw(fond, spaceX8, spaceY8, -rot)
	love.graphics.draw(fond, spaceX9, spaceY9, -rot)
    
	--FIRED lasers
    for i=0, 9 do
		if laserY[i] > -1 then  
			love.graphics.draw(laser, laserX[i], laserY[i], 0, 1,1, laserW/2, 0)
		end
    end

	--THE SHIP!
	if dy > 0 then
		love.graphics.draw(ship2, shipFixX, shipFixY, 0, 1,1, shipW/2, shipH/2 )
	elseif dy < 0 then
		love.graphics.draw(ship3, shipFixX, shipFixY, 0, 1,1, shipW/2, shipH/2 )
	else 
		love.graphics.draw(ship1, shipFixX, shipFixY, 0, 1,1, shipW/2, shipH/2 )
        
        --love.graphics.draw(ship1, shipX, shipY, rot, 1,1, shipW/2, shipH/2 )
	end

    --SPACE OVERGROUND
    --TODO (fond2)
	
	--DISPLAY DEBUG INFORMATION
 	love.graphics.print("Joysticks: " .. joyCount, 10, 20)
	love.graphics.print("Last button: "..lastbutton, 10, 35)
	love.graphics.print(string.format("dx: %.3f ", dx), 10, 50)
	love.graphics.print(string.format("dy: %.3f ", dy), 10, 65)
	love.graphics.print(string.format("dt: %.3f ", ldt), 10, 80) 
  
end


